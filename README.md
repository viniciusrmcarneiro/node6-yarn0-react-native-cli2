# just a container to run in the bitbucket pipeline:
* node: 6.9.4
* yarn: 0.18.1
* react-native-cli: 2.0.1

# here an example how your `bitbucket-pipelines.yml` would looks like:

```yaml
image: viniciusrmcarneiro/node6-yarn0-react-native-cli2
pipelines:
  default:
    - step:
        script:
            - yarn install
            - npm test
```